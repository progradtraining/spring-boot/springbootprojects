package com.example.demo.component;

import org.springframework.stereotype.Component;


@Component
public class Interest {
    private int age=20;
    private double principleAmount=100000;
    private int time=1;

    public Interest(int age, double principleAmount, int time) {
        this.age = age;
        this.principleAmount = principleAmount;
        this.time = time;
    }

    public Interest() {
        this(20,100000,1);
    }

    public void getinterest(){
        double sol;
        if(age>45){
            sol=principleAmount*time*8/100;
            System.out.println(sol);
        }
        else{
            sol=principleAmount*time*4/100;
            System.out.println(sol);
        }
    }

}
