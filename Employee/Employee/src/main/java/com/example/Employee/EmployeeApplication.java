package com.example.Employee;

import com.example.Employee.Components.employee;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class EmployeeApplication {
	static Scanner scanner=new Scanner(System.in);
	public static void main(String[] args) {
		//SpringApplication.run(EmployeeApplication.class, args);
		String ConfigurationFile = "hibernate.cfg.xml";
		ClassLoader classLoaderObj=EmployeeApplication.class.getClassLoader();
		File f = new File(classLoaderObj.getResource(ConfigurationFile).getFile());
		SessionFactory sessionFactoryObj = new Configuration().configure(f).buildSessionFactory();
		Session sessionObj = sessionFactoryObj.openSession();
		//saveRecord(sessionObj);
		//delete(sessionObj);
		System.out.println("To view records press 1");
		System.out.println("To delete record press 2");
		System.out.println("To insert record press 3");
		System.out.println("To update record press 4");
		int option= scanner.nextInt();
		switch (option){
			case 1:
				display(sessionObj);
				break;
			case 2:
				delete(sessionObj);
				break;
			case 3:
				saveRecord(sessionObj);
				break;
			default:
				update(sessionObj);
		}




	}
	public static void display(Session sessionObj){
		Query queryobj=sessionObj.createQuery("FROM employee");
		List<employee> listobj=queryobj.list();
		for (employee iterate:listobj) {
			System.out.println(iterate.getEmpid()+" "+iterate.getEmpname()+" "+iterate.getEmpsalary()+" "+iterate.getDepartment());
		}
	}
	private static void saveRecord(Session sessionObj) {
		employee employee=new employee();
		System.out.println("Enter employee id");
		employee.setEmpid(scanner.nextInt());
		System.out.println("Enter name");
		employee.setEmpname(scanner.next());
		System.out.println("salary");
		employee.setEmpsalary(scanner.nextDouble());
		System.out.println("department");
		employee.setDepartment(scanner.next());
		sessionObj.beginTransaction();
		sessionObj.save(employee);
		sessionObj.getTransaction().commit();
		System.out.println("Record added");

	}
	public static void update(Session sessionobj){
		System.out.println("enter id to update");
		int empid= scanner.nextInt();
		employee employee=(employee) sessionobj.get(employee.class,empid);
		System.out.println("enter employee name");
		employee.setEmpname(scanner.next());
		System.out.println("enter Salary");
		employee.setEmpsalary(scanner.nextDouble());
		System.out.println("Enter department");
		employee.setDepartment(scanner.next());
		sessionobj.beginTransaction();
		sessionobj.saveOrUpdate(employee);
		sessionobj.getTransaction().commit();
		System.out.println("updated records");

	}
	public static  void delete(Session sessionobj){
		System.out.println("enter empid to delete regarding record from table");
		int empid= scanner.nextInt();
		employee student=(employee) sessionobj.get(employee.class,empid);
		sessionobj.beginTransaction();
		sessionobj.delete(student);
		sessionobj.getTransaction().commit();
		System.out.println("updated records");

	}

}
