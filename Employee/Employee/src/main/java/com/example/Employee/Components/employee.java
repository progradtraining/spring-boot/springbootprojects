package com.example.Employee.Components;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employee")
public class employee {
    @Id
    @Column(name="empid")
    private int empid;

    @Column(name="empname")
    private String empname;

    @Column(name="empsalary")
    private double empsalary;

    @Column(name="department")
    private String department;

    public employee() {
    }

    public employee(int empid, String empname, double empsalary, String department) {
        this.empid = empid;
        this.empname = empname;
        this.empsalary = empsalary;
        this.department = department;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public double getEmpsalary() {
        return empsalary;
    }

    public void setEmpsalary(double empsalary) {
        this.empsalary = empsalary;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
